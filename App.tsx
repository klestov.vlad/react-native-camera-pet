import React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import AppNavigator from './src/navigation/navigation';
import {ApolloProvider} from '@apollo/client';
import client from './src/libs/apollo-client/client';

const App: React.FC = () => {
  return (
    <ApolloProvider client={client}>
      <NavigationContainer>
        <AppNavigator />
      </NavigationContainer>
    </ApolloProvider>
  );
};

export default App;
