export enum MainRoutes {
  Home = 'Home',
  Camera = 'Camera',
  UserInfo = 'UserInfo',
  Permissions = 'Permissions',
}
