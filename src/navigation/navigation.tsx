import React from 'react';
import {
  StackNavigationProp,
  createStackNavigator,
} from '@react-navigation/stack';

import {CameraScreen, UserInfoScreen} from '../screens';
import HomeScreen from '../screens/HomeScreen/Home.screen';
import {MainRoutes} from './routes';
import {RouteProp} from '@react-navigation/native';
import {PermissionsPage} from '../screens/PermissionScreen/Permission.screen';

const Stack = createStackNavigator<RootStackParam>();

const AppNavigator: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName={MainRoutes.Home}>
      <Stack.Screen name={MainRoutes.Home} component={HomeScreen} />
      <Stack.Screen name={MainRoutes.Camera} component={CameraScreen} />
      <Stack.Screen name={MainRoutes.UserInfo} component={UserInfoScreen} />
      <Stack.Screen name={MainRoutes.Permissions} component={PermissionsPage} />
    </Stack.Navigator>
  );
};

export default AppNavigator;

export type RootStackParam = {
  [MainRoutes.Home]: undefined;
  [MainRoutes.Camera]: undefined;
  [MainRoutes.UserInfo]: undefined;
  [MainRoutes.Permissions]: undefined;
};

export interface RootStackProps {
  navigation: StackNavigationProp<RootStackParam, MainRoutes.Home>;
  route: RouteProp<RootStackParam, MainRoutes.Home>;
}

export interface CameraStackProps {
  navigation: StackNavigationProp<RootStackParam, MainRoutes.Camera>;
  route: RouteProp<RootStackParam, MainRoutes.Camera>;
}

export interface UserInfoStackProps {
  navigation: StackNavigationProp<RootStackParam, MainRoutes.UserInfo>;
  route: RouteProp<RootStackParam, MainRoutes.UserInfo>;
}

export interface PermissionsStackProps {
  navigation: StackNavigationProp<RootStackParam, MainRoutes.Permissions>;
  route: RouteProp<RootStackParam, MainRoutes.Permissions>;
}
