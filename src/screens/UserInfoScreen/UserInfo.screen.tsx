import React from 'react';
import {View, Text} from 'react-native';

import {useQuery} from '@apollo/client';
import {getFakeUserSchema} from './gql-schemes';
import styles from './UserInfo.styles';

const UserInfoScreen: React.FC = () => {
  const {loading, error, data} = useQuery(getFakeUserSchema);

  if (loading) {
    return (
      <View style={styles.container}>
        <Text>Loading...</Text>
      </View>
    );
  }

  if (error) {
    return (
      <View style={styles.container}>
        <Text>Error: {error.message}</Text>
      </View>
    );
  }

  const {name, address} = data;

  return (
    <View style={styles.container}>
      <Text style={styles.name}>
        {name.firstName} {name.lastName}
      </Text>
      <View style={styles.addressContainer}>
        <Text style={styles.addressLabel}>Address:</Text>
        <Text style={styles.addressText}>{address.streetAddress}</Text>
        <Text style={styles.addressText}>
          {address.city}, {address.country}, {address.zipCode}
        </Text>
      </View>
    </View>
  );
};

export default UserInfoScreen;
