import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
    margin: 10,
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  addressContainer: {
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 10,
  },
  addressLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  addressText: {
    fontSize: 16,
    marginBottom: 5,
  },
});
