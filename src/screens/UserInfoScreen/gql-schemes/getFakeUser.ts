import {gql} from '@apollo/client';

export const getFakeUserSchema = gql`
  query {
    name {
      firstName
      lastName
    }
    address {
      city
      country
      zipCode
      timeZone
      streetName
      streetAddress
    }
  }
`;
