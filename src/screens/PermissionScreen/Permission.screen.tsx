import React, {FC, useCallback, useEffect, useState} from 'react';
import {Linking} from 'react-native';

import {View, Text} from 'react-native';
import {Camera, CameraPermissionStatus} from 'react-native-vision-camera';
import {PermissionsStackProps} from '../../navigation/navigation';
import {MainRoutes} from '../../navigation/routes';
import styles from './Permission.styles';

export const PermissionsPage: FC<PermissionsStackProps> = ({navigation}) => {
  const [cameraPermissionStatus, setCameraPermissionStatus] =
    useState<CameraPermissionStatus>('not-determined');

  const requestCameraPermission = useCallback(async () => {
    console.log('Requesting camera permission...');
    const permission = await Camera.requestCameraPermission();
    console.log(`Camera permission status: ${permission}`);

    if (permission === 'denied') await Linking.openSettings();
    setCameraPermissionStatus(permission);
  }, []);

  useEffect(() => {
    if (cameraPermissionStatus === 'authorized') {
      navigation.replace(MainRoutes.Camera);
    }
  }, [cameraPermissionStatus, navigation]);

  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        The app needs{'\n'} access to the camera.
      </Text>
      <View style={styles.permissionsContainer}>
        {cameraPermissionStatus !== 'authorized' && (
          <Text style={styles.permissionText}>
            To allow access to the <Text style={styles.bold}>camera</Text>.{' '}
            <Text style={styles.hyperlink} onPress={requestCameraPermission}>
              Click here
            </Text>
          </Text>
        )}
      </View>
    </View>
  );
};
