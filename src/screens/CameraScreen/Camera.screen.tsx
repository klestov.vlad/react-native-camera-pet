import React, {FC, useEffect, useRef} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';

import {
  Camera,
  CameraPermissionStatus,
  useCameraDevices,
} from 'react-native-vision-camera';
import {CameraStackProps} from '../../navigation/navigation';
import {MainRoutes} from '../../navigation/routes';
import RNFS from 'react-native-fs';
import styles from './Camera.styles';

const CameraComponent: FC<CameraStackProps> = ({navigation}) => {
  const [cameraPermission, setCameraPermission] = React.useState<
    CameraPermissionStatus | undefined
  >(undefined);

  const devices = useCameraDevices();
  const device = devices.back;

  const cameraRef = useRef<Camera>(null);

  const checkPermissions = async () => {
    setCameraPermission(await Camera.getCameraPermissionStatus());
  };

  useEffect(() => {
    checkPermissions();
  }, []);

  useEffect(() => {
    if (cameraPermission && cameraPermission !== 'authorized') {
      navigation.navigate(MainRoutes.Permissions);
    }
  }, [cameraPermission, navigation]);

  const takePhoto = async () => {
    if (cameraRef.current) {
      const photo = await cameraRef.current.takePhoto();
      savePhoto(photo.path);
    }
  };

  const savePhoto = async (photoUri: string) => {
    try {
      const currentDate = new Date();
      const timestamp = currentDate.getTime();
      const filename = `photo_${timestamp}.jpg`;
      const destinationPath = `${RNFS.DocumentDirectoryPath}/photos/${filename}`;
      await RNFS.moveFile(photoUri, destinationPath);
      console.log(destinationPath, 'Photo saved to:', destinationPath);
    } catch (error) {
      console.log('Failed to save photo:', error);
    }
  };

  return (
    <View style={styles.container}>
      {device ? (
        <Camera
          ref={cameraRef}
          style={styles.camera}
          focusable
          device={device}
          isActive
        />
      ) : (
        <View style={styles.emptyContainer} />
      )}
      <TouchableOpacity onPress={takePhoto} style={styles.takePhotoButton}>
        <Text style={styles.takePhotoText}>Take Photo</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CameraComponent;
