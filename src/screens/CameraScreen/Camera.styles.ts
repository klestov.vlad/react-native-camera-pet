import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  takePhotoButton: {position: 'absolute', bottom: 20, alignSelf: 'center'},
  takePhotoText: {fontSize: 18, color: 'white'},
  camera: {flex: 1},

  emptyContainer: {flex: 1, backgroundColor: 'black'},
});
