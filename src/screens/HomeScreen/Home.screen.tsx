import React, {FC} from 'react';
import {View, Button} from 'react-native';
import {RootStackProps} from '../../navigation/navigation';
import {MainRoutes} from '../../navigation/routes';
import styles from './Home.styles';

const HomeScreen: FC<RootStackProps> = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Button
        title="Camera"
        onPress={() => navigation.navigate(MainRoutes.Camera)}
      />
      <Button
        title="User Info"
        onPress={() => navigation.navigate(MainRoutes.UserInfo)}
      />
    </View>
  );
};

export default HomeScreen;
